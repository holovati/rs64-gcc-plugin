#include <cereal/types/map.hpp>
#include <cereal/types/vector.hpp>
#include <cereal/types/memory.hpp>
#include <cereal/archives/json.hpp>
#include <map>
#include <fstream>

#include "gcc-plugin.h"
#include "plugin-version.h"
#include "cp/cp-tree.h"
#include "print-tree.h"
#include "opts.h"

#include <stdio.h>
#include <assert.h>

struct FunctionParameter
{
    std::string type;
    std::string name;
    bool is_pointer;

    void FillFromTree(tree parm_decl)
    {
        //debug_tree(parm_decl);
        assert(TREE_CODE(parm_decl) == PARM_DECL);
        is_pointer = TREE_CODE(TREE_TYPE(parm_decl)) == POINTER_TYPE;
        type = type_as_string(TREE_TYPE(parm_decl), TFF_DECL_SPECIFIERS);
        name = decl_as_string(DECL_NAME(parm_decl), TFF_DECL_SPECIFIERS);
#ifdef notyet
        for (tree attr = DECL_ATTRIBUTES(parm_decl); attr; attr = TREE_CHAIN(attr))
        {
            tree attrname = TREE_PURPOSE(attr);
            tree attrargs = TREE_VALUE(attr);
        }
#endif
    }

    template <class Archive>
    void serialize(Archive &ar)
    {
        ar(CEREAL_NVP(type));
        ar(CEREAL_NVP(name));
        ar(CEREAL_NVP(is_pointer));
    }
};

struct MemberFunction
{
    std::string name;
    std::string file_name;
    std::string assembler_name;
    std::string plain_identifier;
    std::string unqualified_name;
    std::string return_type;
    std::string linux_syscall;
    std::string access;
    std::vector<FunctionParameter> parameters;
    bool is_static;

    void FillFromTree(tree fndecl)
    {
        //debug_tree(fndecl);
        tree fnattr = DECL_ATTRIBUTES(fndecl);
        tree attrargs = TREE_VALUE(fnattr);

        for (tree attr = attrargs; attr; attr = TREE_CHAIN(attr))
        {
            linux_syscall = TREE_STRING_POINTER(DECL_NAME(attr));
        }

        //debug_tree(fndecl);

        name = decl_as_string(fndecl, TFF_UNQUALIFIED_NAME | TFF_NO_FUNCTION_ARGUMENTS);
        plain_identifier = decl_as_string(fndecl, TFF_PLAIN_IDENTIFIER | TFF_NO_FUNCTION_ARGUMENTS);
        //return_type = IDENTIFIER_POINTER(DECL_NAME(TYPE_NAME(fndecl_declared_return_type(fndecl))));
        unqualified_name = decl_as_string(fndecl, TFF_RETURN_TYPE | TFF_UNQUALIFIED_NAME);
        return_type = type_as_string(fndecl_declared_return_type(fndecl), TFF_DECL_SPECIFIERS);

        file_name = DECL_SOURCE_FILE(fndecl);
        assembler_name = decl_as_string(DECL_ASSEMBLER_NAME(fndecl), TFF_PLAIN_IDENTIFIER);
        //is_member = DECL_FUNCTION_MEMBER_P(fndecl);
        is_static = DECL_STATIC_FUNCTION_P(fndecl);
        access = TREE_PRIVATE(fndecl) ? "private" : TREE_PROTECTED(fndecl) ? "protected" : "public";
        //debug_tree(DECL_CONTEXT(TYPE_FIELDS(fnclass)));

        for (tree parm_decl = FUNCTION_FIRST_USER_PARM(fndecl); parm_decl; parm_decl = TREE_CHAIN(parm_decl))
        {
            FunctionParameter fnparm;

            fnparm.FillFromTree(parm_decl);

            parameters.push_back(fnparm);
        }
    }

    template <class Archive>
    void serialize(Archive &ar)
    {
        ar(CEREAL_NVP(name));
        ar(CEREAL_NVP(file_name));
        ar(CEREAL_NVP(assembler_name));
        ar(CEREAL_NVP(plain_identifier));
        ar(CEREAL_NVP(unqualified_name));
        ar(CEREAL_NVP(return_type));
        ar(CEREAL_NVP(linux_syscall));
        ar(CEREAL_NVP(is_static));
        ar(CEREAL_NVP(access));
        ar(CEREAL_NVP(parameters));
    }
};

struct CFunction
{
    struct CFunctionParam
    {
        std::string type;
        std::string name;
        std::string fullname;
        bool is_pointer;

        template <class Archive>
        void serialize(Archive &ar)
        {
            ar(CEREAL_NVP(type));
            ar(CEREAL_NVP(name));
            ar(CEREAL_NVP(fullname));
            ar(CEREAL_NVP(is_pointer));
        }
    };

    std::string name;
    std::string linux_name;
    std::string return_type;
    std::string header;

    std::vector<CFunctionParam> parameters;

    template <class Archive>
    void serialize(Archive &ar)
    {
        ar(CEREAL_NVP(name));
        ar(CEREAL_NVP(linux_name));
        ar(CEREAL_NVP(return_type));
        ar(CEREAL_NVP(header));
        ar(CEREAL_NVP(parameters));
    }
};

static bool
is_syscall_method(tree class_field)
{
    // Only functions and methods can be syscalls
    if (!DECL_DECLARES_FUNCTION_P(class_field))
    {
        return false;
    }

    tree fnattr = DECL_ATTRIBUTES(class_field);
    // No attributes, not a syscall
    if (!fnattr)
    {
        return false;
    }

    tree fnattrname = TREE_PURPOSE(fnattr);

    if (strcmp(IDENTIFIER_POINTER(fnattrname), "linux_syscall"))
    {
        // No syscall attribute;
        return false;
    }
    //debug_tree(fndecl);

    return true;
}

static tree first_syscall_decl(tree class_field)
{
    while (class_field)
    {
        if (is_syscall_method(class_field))
        {
            //debug_tree(class_field);
            break;
        }
        class_field = TREE_CHAIN(class_field);
    }
    return class_field;
}

static tree next_syscall_decl(tree class_field)
{
    class_field = TREE_CHAIN(class_field);
    return first_syscall_decl(class_field);
}

using MethodArray = std::vector<MemberFunction>;

struct clazz
{
    std::string name;
    std::string file_name;
    std::string full_name;

    MethodArray methods;

    void FillFromTree(tree class_decl)
    {
        //debug_tree(class_decl);
        name = (IDENTIFIER_POINTER(DECL_NAME(TYPE_NAME(class_decl))));
        file_name = DECL_SOURCE_FILE(TYPE_NAME(class_decl));
        full_name = decl_as_string(class_decl, TFF_DECL_SPECIFIERS | TFF_RETURN_TYPE);

        tree fndecl = TYPE_FIELDS(class_decl);

        for (fndecl = first_syscall_decl(fndecl);
             fndecl;
             fndecl = next_syscall_decl(fndecl))
        {
            methods.emplace_back();
            methods.back().FillFromTree(fndecl);
        }
    }

    template <class Archive>
    void serialize(Archive &ar)
    {
        ar(CEREAL_NVP(name));
        ar(CEREAL_NVP(file_name));
        ar(CEREAL_NVP(full_name));
        ar(CEREAL_NVP(methods));
    }
};

using ClassMap = std::map<std::string, clazz>;
using ClassMapPair = std::pair<ClassMap::iterator, bool>;

struct dump_data
{
    std::vector<clazz> class_array;
    const char *header_in = nullptr;
    const char *json_out = nullptr;
    bool verbose = false;

#if 0
    template <class Archive>
    void serialize(Archive &ar)
    {
        ar(cereal::make_nvp("header_file", std::string(header_in)));
        ar(cereal::make_nvp("classes", class_array));
    }
#endif
    std::map<std::string, CFunction> c_funcs;

    template <class Archive>
    void serialize(Archive &ar)
    {
        ar(cereal::make_nvp("syscalls", c_funcs));
    }
};

// https://code.woboq.org/gcc/gcc/plugin.def.html
int plugin_is_GPL_compatible;

static void type_decl_finished(void *gcc_data, void *user_data)
{
    dump_data *data = (dump_data *)user_data;

    tree type_decl = (tree_node *)gcc_data;

    if (TREE_CODE(type_decl) != FUNCTION_DECL)
    {
        return;
    }

    if (!is_syscall_method(type_decl))
    {
        return;
    }

    std::string linux_sysname;

    tree fnattr = DECL_ATTRIBUTES(type_decl);
    tree attrargs = TREE_VALUE(fnattr);
    for (tree attr = attrargs; attr; attr = TREE_CHAIN(attr))
    {
        linux_sysname = TREE_STRING_POINTER(DECL_NAME(attr));
    }

    std::map<std::string, CFunction>::iterator it = data->c_funcs.find(linux_sysname);

    if (it == data->c_funcs.end())
    {
        it = data->c_funcs.insert(std::pair<std::string, CFunction>(linux_sysname, CFunction{})).first;
    }
    else
    {
        (*it).second.parameters.clear();
    }

    (*it).second.name = decl_as_string(type_decl, TFF_UNQUALIFIED_NAME | TFF_NO_FUNCTION_ARGUMENTS);
    (*it).second.return_type = type_as_string(fndecl_declared_return_type(type_decl), TFF_DECL_SPECIFIERS);
    (*it).second.header = DECL_SOURCE_FILE(type_decl);
    (*it).second.linux_name = linux_sysname;

    for (tree parm_decl = FUNCTION_FIRST_USER_PARM(type_decl); parm_decl; parm_decl = TREE_CHAIN(parm_decl))
    {
        //debug_tree(TREE_TYPE(TREE_TYPE(parm_decl)));
        assert(TREE_CODE(parm_decl) == PARM_DECL);

        (*it).second.parameters.push_back({
            type_as_string(TREE_TYPE(parm_decl), TFF_DECL_SPECIFIERS),
            decl_as_string(DECL_NAME(parm_decl), TFF_DECL_SPECIFIERS),
            type_as_string(TREE_TYPE(TREE_TYPE(parm_decl)), TFF_PLAIN_IDENTIFIER),
            TREE_CODE(TREE_TYPE(parm_decl)) == POINTER_TYPE,

        });
    }

    //debug_tree((type_decl));
    //printf("-------------------------------\n");
}

static void record_decl_finished(void *gcc_data, void *user_data)
{
    dump_data *data = (dump_data *)user_data;

    tree type_decl = (tree_node *)gcc_data;

    /* Disable c++ class syscalls for now */
    return;

    if (TREE_CODE(type_decl) != RECORD_TYPE)
    {
        return;
    }

    if (!CLASS_TYPE_P(type_decl))
    {
        return;
    }

    if (strcmp(DECL_SOURCE_FILE(TYPE_NAME(type_decl)), data->header_in))
    {
        // This class in not declared in the header we are processing it's a #include(d) type
        return;
    }

    if (data->verbose)
    {
        printf("-------------------------------\n");
        debug_tree(type_decl);
    }

    data->class_array.emplace_back();
    data->class_array.back().FillFromTree(type_decl);

    if (data->class_array.back().methods.size() == 0)
    {
        data->class_array.pop_back();
    }
}

static void register_attributes(void *event_data, void *data)
{
    static struct attribute_spec linux_syscall_attr = {
        "linux_syscall",
        1,
        1,
        false,
        false,
        false,
        [](tree *, tree, tree, int, bool *) -> tree {
            return NULL_TREE;
        }};

    //warning(0, "Callback to register attributes");
    register_attribute(&linux_syscall_attr);
#ifdef notyet
    static struct attribute_spec syscall_instance_attr = {
        "syscall_instance",
        0,
        1,
        false,
        false,
        false,
        [](tree *, tree, tree, int, bool *) -> tree {
            return NULL_TREE;
        }};

    register_attribute(&syscall_instance_attr);
#endif
}

static void on_exit(void *event_data, void *data)
{
    dump_data *dump = (dump_data *)data;

    if (1 || dump->verbose)
    {
        cereal::JSONOutputArchive archive(std::cout);
        dump->serialize(archive);
    }
    else
    {

        std::ofstream output_stream(dump->json_out);

        cereal::JSONOutputArchive file_archive(output_stream);

        dump->serialize(file_archive);
    }
    delete dump;
}

int plugin_init(struct plugin_name_args *plugin_info,
                struct plugin_gcc_version *version)
{
    if (!plugin_default_version_check(version, &gcc_version))
    {
        printf("This GCC plugin is for version %d.%d\n", GCCPLUGIN_VERSION_MAJOR, GCCPLUGIN_VERSION_MINOR);
        return 1;
    }

    dump_data *data = new dump_data;

#if 0
    for (int i = 0; i < plugin_info->argc; i++)
    {
        printf("Argument %d Key: %s Value: %s\n", i, plugin_info->argv[i].key, plugin_info->argv[i].value);
        if (strcmp(plugin_info->argv[i].key, "header-in") == 0)
        {
            data->header_in = plugin_info->argv[i].value;
            continue;
        }

        if (strcmp(plugin_info->argv[i].key, "json-out") == 0)
        {
            data->json_out = plugin_info->argv[i].value;
            continue;
        }

        if (strcmp(plugin_info->argv[i].key, "verbose") == 0)
        {
            data->verbose = true;
            continue;
        }
    }

    if (data->header_in == nullptr)
    {
        error("input header not specified");
        delete data;
        return 1;
    }

    if (data->json_out == nullptr)
    {
        delete data;
        error("json output file not specified");
        return 1;
    }
#endif
    register_callback(plugin_info->base_name,
                      PLUGIN_FINISH,
                      on_exit,
                      data);

    register_callback(plugin_info->base_name,
                      PLUGIN_FINISH_TYPE,
                      record_decl_finished,
                      data);

    register_callback(plugin_info->base_name,
                      PLUGIN_ATTRIBUTES,
                      register_attributes,
                      data);
    // For free standing functions
    register_callback(plugin_info->base_name,
                      PLUGIN_FINISH_DECL,
                      type_decl_finished,
                      data);

    if (data->verbose)
    {
        printf("Plugin info\n");
        printf("===========\n\n");
        printf("Base name: %s\n", plugin_info->base_name);
        printf("Full name: %s\n", plugin_info->full_name);
        printf("Number of arguments of this plugin: %d\n", plugin_info->argc);

        if (plugin_info->version != NULL)
            printf("Version string of the plugin: %s\n", plugin_info->version);
        if (plugin_info->help != NULL)
            printf("Help string of the plugin: %s\n", plugin_info->help);

        printf("\n");
        printf("Version info\n");
        printf("============\n\n");
        printf("Base version: %s\n", version->basever);
        printf("Date stamp: %s\n", version->datestamp);
        printf("Dev phase: %s\n", version->devphase);
        printf("Revision: %s\n", version->devphase);
        printf("Configuration arguments: %s\n", version->configuration_arguments);
        printf("\n");

        printf("Plugin successfully initialized\n");
    }

    return 0;
}