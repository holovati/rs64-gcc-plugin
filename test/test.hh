#include <sys/types.h>
typedef unsigned short		umode_t;

#ifdef SYSCALL_GENERATOR
#define SYSCALL(x) __attribute__((linux_syscall(#x)))
#define SYSCALL_INSTANCE /* __attribute__((syscall_instance)) */
#else
#define SYSCALL(x) /* SYSCALL x */
#define SYSCALL_INSTANCE /* SYSCALL INSTANCE */
#endif

namespace shabaz_space {
class Process {
    private:
    static  SYSCALL(read) int* ReadFile(int const * abe);
    public:

    static Process* SYSCALL_INSTANCE GetInstance();
    int SYSCALL(open) OpenFile(const char *filename, int flags, umode_t mode);

    static SYSCALL(mprotect) 
    int MemoryProtect(unsigned long start, size_t len, unsigned long prot);
    #include "test_syscall_header.h"
};
}